import java.util.Scanner;

public class VirtualPetApp {
    public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Fox[] skulk = new Fox[1];
	
		for(int i=0; i<skulk.length; i++){
			System.out.println("\r\n"+ "--Fox " +(i+1)+ "--");
			System.out.println("Gimme the name of ur fox");
            String name = reader.nextLine();

            System.out.println("Gimme the habitat of ur fox");
            String habitat = reader.nextLine();

            System.out.println("Gimme the size of ur fox (in cm)");
            int size = reader.nextInt();
            reader.nextLine();

            System.out.println("Gimme a word to describe ur fox's vibe");
            String vibe = reader.nextLine();
			
            skulk[i] = new Fox(name, habitat, size, vibe);
		}
		
		//System.out.println("changing value of size: " + skulk[0].getName());
		//skulk[0].setname(50);
		//System.out.println("to new size: " + skulk[0].getSize());
		
		System.out.println("give me a new name");
		String newValue = reader.nextLine();
		skulk[skulk.length -1].setName(newValue);
		System.out.println("new value: " + skulk[skulk.length -1].getName());
		
    }
}