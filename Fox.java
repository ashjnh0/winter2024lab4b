public class Fox{
	private String name;
	private String habitat;
	private int size;
	private String vibe;

	public Fox(String name, String habitat, int size, String vibe){
		this.name = name;
		this.habitat = habitat;
		this.size = size;
		this.vibe = vibe;
	}
	
	//setters and getters
	public void setName(String name){
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getHabitat() {
		return this.habitat;
	}
	
	public int getSize() {
		return this.size;
	}
	
	public String getVibe() {
		return this.vibe;
	}
	
	public String animalVibe(){
		return "This " +this.name+ " fox has a(n) "+this.vibe+ " vibe";
	}

	public String animalCard(){
		return "\r\n"+ "		[Random Fox Card]" +"\r\n"+ "Name: " +this.name+ "\r\n" + "Habitat: " +this.habitat+"\r\n"+"Size (in cm): " +this.size;
	}
}